var Resource = require('./node_modules/deployd/lib/resource')
  , util = require('util');

function Hello2(settings) {
    // console.log('Hello2 INITIALIZED')
  Resource.apply(this, arguments);
}
util.inherits(Hello2, Resource);
module.exports = Hello2;
module.exports.staticTest = function(a) {
    return a * 2;
}

Hello2.prototype.handle = function (ctx, next) {
  if(ctx.req && ctx.req.method !== 'GET') return next();
  
  ctx.done(null, {hello2: 'world2'});
};