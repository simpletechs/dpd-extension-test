expect = require('chai').expect;

var testTargetFn = require('../index').staticTest;
describe('static test', function() {
    it('should return a*2', function() {
        var t = 5;
        expect(testTargetFn(t)).to.equal(t * 2);
    });
})