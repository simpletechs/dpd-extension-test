describe('dpd-extension-test', function() {
  describe('dynamic test', function() {
    it('should return "hello2"', function(done) {
      dpd('hello2').get().then(function(res) {
        expect(res).to.exist;
        expect(res.hello2).to.equal('world2');

        done();
      }, function(err) {
        done(err);
      });
    });
  });
});
